Magento Transactional Email Translations
=========================================

Just a bunch of translations of Magento's transactional email templates.

Should be placed in `app/locale/[your locale]/template`